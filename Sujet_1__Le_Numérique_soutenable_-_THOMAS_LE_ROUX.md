Sujet 1: Le Numérique soutenable - THOMAS LE ROUX
====

Introduction
----
Il est vrai que depuis la naissance du numérique, son impact environnemental a augmenté de manière considérable. De nos jours, l’un des plus gros problèmes venant du numérique est sa consommation d'énergie importante, qui, d'après un article de la tribune du 18 décembre 2018, représente entre 6% et 10% de la consommation globale, ce qui entraîne presque 5% de l'empreinte carbone globale aussi, comme le dit Sylvain Rolland, l’auteur de l’article, le numérique “serait le troisième plus gros consommateur d'électricité au monde, derrière la Chine et les États-Unis”[[1]](https://www.latribune.fr/technos-medias/internet/comment-le-numerique-pollue-dans-l-indifference-generale-801385.html). 

L'article
-----
En vue de ce problème qui passe inaperçu par par une grande majorité des gens, l’Arcep (L’Autorité de Régulation des Communications Électroniques et des Postes) [[2]](https://www.arcep.fr) décide qu’il faut passer à l’action immédiatement. Dans un article sur NextInpact du 6 Janvier 2021, Sébastien Gavois, le président de l’Arcep met en place son plan à trois étapes qui d'après lui serait capable de combattre, en grande partie, l’impact environnemental du numérique [[3]](https://www.nextinpact.com/article/45093/pour-numerique-soutenable-bonnes-intentions-ce-nest-pas-suffisant).

Dans son plan, il propose de changer de nombreux aspects de notre utilisation du numérique pour réduire son empreinte carbone, mais ce qui n’est peut-être pas évident à première vue, c’est le fait que ce qu’il suggère se rapproche beaucoup au capitalisme de surveillance. 

Capitalisme de surveillance 
-----
Mais qu'est ce que le capitalisme de surveillance? C’est un concept introduit après l’affaire Snowden en 2014 [[4]](https://en.wikipedia.org/wiki/Edward_Snowden), dans laquelle Edward Snowden, un consultant pour la NSA (National Security Agency), a dévoiler de nombreux documents top-secret, révélant une politique de surveillance en masse aux États-Unis. En effet, cette notion de capitalisme de surveillance est principalement liée aux États-Unis, avec une surveillance globale publicitaire, des flux bancaires et même des populations, ce qu’on appelle une “société de contrôle”. Mais les États-Unis ne sont pas les seuls concernés par cette surveillance, il se trouve que nous en sommes victime aussi en France. En 1974, l’affaire Safari dans laquelle le ministre de l'intérieur se voit accusé de retenir des informations sur les français en lien avec l’identité et la sécurité sociale [[5]](http://360objets.fr/2018/05/affaire-safari-1974-origine-loi-informatique-et-libertes/). Cette affaire a donc amené la France à créer le CNIL (Commission Nationale de l'informatique et des Libertés), qui n’a cependant pas été très efficace.

Le lien avec l'article
----
Il y donc une certaine histoire de surveillance en France, et ce qui propose Sébastien Gavois n’est pas très différent, en effet en regardant les trois grand axes de changements qu’il souhaiterait établir,  dès la première ligne, il parle de “capacité de pilotage” par les pouvoirs publics, dans ce cas précis il parlait l’empreinte numérique certes, mais de pilotage quand même. Cela voudrait donc dire que les pouvoirs publics auraient un plus grand contrôle sur ce qu’il se passe sur Internet. Dans son deuxième point, il parle également d'intégrer “l’enjeu environnemental dans les actions de régulation de l’Arche”, ce qui leur donnerait plus de pouvoir. Et finalement, dans son troisième axe, il parle de “renforcer les incitations des acteurs économiques, acteurs privés, publics et consommateurs”, c’est à dire se rapprocher des grandes compagnies comme les GAFAM, dans certaines d’entre elles sont connues pour la surveillance et vente de données personnelles comme pour l’affaire de Facebook en 2016 [[6]](https://en.wikipedia.org/wiki/Facebook–Cambridge_Analytica_data_scandal), dans laquelle la compagnie Facebook a été accusée d’avoir obtenu de manière illégale de nombreuses informations sur les utilisateurs du site. Dans son article, il parle aussi du fait qu’il ne s'agit pas d'empêcher le développement du numérique, mais à aucun moment ne parle-t-il du contrôle du numérique, et il accentue bien le fait qu’il faut absolument obtenir des résultats et non pas uniquement des moyens. 

Conclusion 
---
Ce Plan proposé par Sébastien Gavois à pour but officiel de réduire l’impact environnemental du numérique, mais ce qu’il propose ressemble aussi beaucoup à de la surveillance du web, à travers ces propositions ambiguës qui donneraient plus de contrôle aux acteurs privés et publiques, il se pourrait que la France se dirige vers une société de contrôle. 

Bibliographie 
[1] : https://www.latribune.fr/technos-medias/internet/comment-le-numerique-pollue-dans-l-indifference-generale-801385.html
[2] : https://www.arcep.fr
[3] : https://www.nextinpact.com/article/45093/pour-numerique-soutenable-bonnes-intentions-ce-nest-pas-suffisant
[4] : https://en.wikipedia.org/wiki/Edward_Snowden
[5] : http://360objets.fr/2018/05/affaire-safari-1974-origine-loi-informatique-et-libertes/
[6] : https://en.wikipedia.org/wiki/Facebook–Cambridge_Analytica_data_scandal

Licence : http://creativecommons.org/licenses/by-sa/2.0/fr/ La licence creative commons permet de partager et de diffuser (copier, Distribuer, et communiquer le matériel) par tous les moyens et sous tous les formats.